# Configure dev Ubuntu machines

An Ansible role to ensure
* user can `sudo` without password
* user can `docker` without `sudo`
* Python, according to [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices)
  * install Pyenv, for Python version management
  * install Python 3.11 and set as default using Pyenv
  * install Python Poetry
* Javascript, according to [Javascript best practices](https://code.europa.eu/digit-c4/dev/javascript-best-practices)
  * install NVM, for node version management,
  * install Node 20
* Proxy
  * configure .bashrc to set web proxy for all sessions
  * configure .ssh on code.europa.eu
  * configure web proxy for NPM
* Devops:
  * Terraform, Terragrunt, terraform-docs

## Variables
| name                  | description                                              |
|-----------------------|----------------------------------------------------------| 
| `proxy_host`          | the fqdn of the proxy host                               |
| `proxy_port`          | the proxy port                                           |
| `proxy_username`      | the username for the proxy                               |
| `proxy_password`      | password associated to `proxy_username`                  |
| `dev_username`        | the username of the dev user of the VM                   |
| `ansible_become_pass` | you know what. Only required if sudo requires a password |
