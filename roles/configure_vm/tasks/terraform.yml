- name: Install Hashicorp Vault CLI and Terraform
  block:
    - name: ensures GPG is present
      become: yes
      ansible.builtin.apt:
        name: gpg
        state: present

    - name: Add GPG key
      become: yes
      environment:
        http_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}
        https_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}
      ansible.builtin.get_url:
        url: https://apt.releases.hashicorp.com/gpg
        dest: /usr/share/keyrings/hashicorp-archive-keyring.asc
        mode: "0644"
        force: true

    - name: Add repo
      become: yes
      ansible.builtin.apt_repository:
        filename: hashicorp
        repo: "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.asc] https://apt.releases.hashicorp.com {{ ansible_distribution_release }} main"
      environment:
        http_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}
        https_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}

    - name: install packages
      become: yes
      ansible.builtin.apt:
        name: "{{ item }}"
        state: present
      loop:
        - terraform
        - vault

- name: Install Terraform-doc
  become: yes
  environment:
    http_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}
    https_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}
  ansible.builtin.unarchive:
    src: https://terraform-docs.io/dl/v0.16.0/terraform-docs-v0.16.0-Linux-amd64.tar.gz
    dest: /usr/local/bin
    remote_src: yes
    owner: root
    group: root
    mode: '0755'

- name: Install Terragrunt
  block:
    - name: check CLI already installed
      ansible.builtin.shell: which terragrunt
      become_user: "{{ dev_username }}"
      register: terragrunt_check
      # never fail and never changed
      failed_when: terragrunt_check.rc < 0
      changed_when: terragrunt_check.rc < 0

    # could also potentially install with brew
    #- name: install Brew
    #  ansible.builtin.shell: NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    #  become: yes
    #  become_user: "{{ dev_username }}"

    - name: install CLI if needed
      when: terragrunt_check.rc > 0
      become: yes
      environment:
        http_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}
        https_proxy: http://{{ proxy_username | urlencode }}:{{ proxy_password | urlencode }}@{{ proxy_host }}:{{ proxy_port }}
      ansible.builtin.get_url:
        url: https://github.com/gruntwork-io/terragrunt/releases/download/{{ terragrunt_version }}/terragrunt_linux_amd64
        dest: /usr/local/bin/terragrunt
        mode: "0755"
        force: true
