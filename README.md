# Ansible collection for dev

## Usage
List the collection in the requirements.yml
```yml
# requirements.yml
collections:
  - name: nms.dev
    source: https://code.europa.eu/digit-c4/dev/ansible-collection.git
    type: git
    version: main
```
run
```shell
ansible-galaxy collection install -r requirements.yml
```
> ⚠️ You may need to add `--force` to upgrade

## Content
* Roles
  * [**configure_vm**](roles/configure_vm): install the default packages on the development Ubuntu VMs.
    an [example](example) execution playbook and procedure is provider for NMS dev to run the procedure alone.
