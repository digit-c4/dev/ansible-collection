# Dev VM initialization

provides an example usage, potentially helpful for devs looking for a way to initialize their dev VM!

## Connect to the VM
Open a Windows PowerShell session
```Powershell
# Set the server host
Set-Variable -Name "DEV_VM_HOST" -Value "lab-${Env:UserName}.dev..............int"
# Remove any previous known key
ssh-keygen -R $DEV_VM_HOST
# Generate a local private key pair, if none
ssh-keygen
# Upload the local public key on the server
type ${env:USERPROFILE}\.ssh\id_rsa.pub | ssh $DEV_VM_HOST "cat >> .ssh/authorized_keys"
# Connect to the new server
ssh $DEV_VM_HOST
```

## Requirements
```shell
# Web proxy must be configured
set +o history
export http_proxy=...
export https_proxy=$http_proxy
set -o history
# ansible installed
python3 -m pip install --user ansible
source ~/.profile
# check ansible was installed
ansible --version
# Have git available to clone this project
sudo apt-get install git
```

## Usage
The first step is to create an SSH key pair on the lab. This stage cannot be automated as we want to create a
passwordless key pair, and the `ssh-keygen -N ""` fail on our VMs. Question was raised to SYS team as this command
is known to be working elsewhere.
```shell
# Manually create the SSH key pair
ssh-keygen
```

Then execute the example test playbook
```shell
# Prepare project structure
PROJECT_PATH=Workspace/code.europa.eu/digit-c4/dev
mkdir -p /tmp/$PROJECT_PATH && cd /tmp/$PROJECT_PATH
# Clone this repo and move to the example dir
git clone https://code.europa.eu/digit-c4/dev/ansible-collection.git \
 && cd ansible-collection/example
# fill your own secret values in the secret yaml file
mv group_vars/all/secret.example.yml group_vars/all/secret.yml
nano group_vars/all/secret.yml
# install the collection (if that fails add export PATH=$HOME/.local/bin:$HOME/bin:/usr/local/bin:$PATH in your ~/.bashrc or ~/.zshrc file depending if you use bash or zsh)
ansible-galaxy collection install --force ../
# and execute
ansible-playbook test.playbook.yml
# Last step is to move this repo to the ~/Workspace direectory
mkdir -p $HOME/$PROJECT_PATH && mv /tmp/$PROJECT_PATH/ansible-collection $HOME/$PROJECT_PATH \
 && cd $HOME/$PROJECT_PATH/ansible-collection
```
